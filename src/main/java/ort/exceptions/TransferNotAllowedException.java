package ort.exceptions;

public class TransferNotAllowedException extends Exception {
    public TransferNotAllowedException(String cause) {
        super(cause);
    }
}
