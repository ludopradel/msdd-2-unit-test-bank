package ort;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private List<Account> accounts;
    private final String name;

    public Customer (String name) {
        this.name = name;
        accounts = new ArrayList<>();
    }

    public Account createAccount(AccountType accountType) {
        Account currentAccount = new Account(accountType, this);
        accounts.add(currentAccount);
        return currentAccount;
    }
}
